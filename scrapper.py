from bs4 import BeautifulSoup
import requests
import re
import sys

def scrapper(url):
    # main scrapper check what scrapper function we must to use
    # must to return a dict with the next data:
        # dict_default = {
        #     "title": "",
        #     "subtitle": "",
        #     "price": "-",
        #     "images": [""],
        #     "info_basic": {
        #         "rooms": "-",
        #         "baths": "-",
        #         "garages": "-",
        #         "orientation": "-"
        #     },
        #     "meters": {
        #         "total": "-",
        #         "cub": "-"
        #     },
        #     "ambients": "-",
        #     "addicionals": "-",
        #     "description": "-",
        # }
    dict_response = {"Error": False, "message": "Error al cargar URL pruebe con otra"}
    try:
        if url.split("/")[2] == "www.compraensanjuan.com":
            dict_response = scrapper_comprasensanjaun(url)
        
        if url.split("/")[2] == "www.redinmo.com.ar":
            dict_response = scrapper_redinmo(url)
    except:
        e = sys.exc_info()[0]
        dict_response
    
    return dict_response

def scrapper_comprasensanjaun(url):
    # scrap comprasensanjuan.com
    dict_response = {}
    response = requests.get(url)
    soup = BeautifulSoup(response.text, "html.parser")
    dict_response["title"] = soup.select_one(
        "#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > h2").text.strip()

    dict_response["subtitle"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-4.col-md-4.col-sm-12.col-12.info-principal-col > div.row.align-items-center > div > div > p.ubicacion.m-0 > span").text.strip()

    dict_response["price"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-4.col-md-4.col-sm-12.col-12.info-principal-col > h3.text-center.precio").text.strip()
    dict_response["condition"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.container.mb-2 > div > div.col-lg-8.col-md-8.col-sm-8.col-9 > p > a:nth-child(6)").text.strip()

    # get images URLs
    images = []
    images.extend(soup.select(".others-img-anuncio"))
    dict_response["images"] = []
    for img in images:
        src = img.attrs["src"][2:]
        dict_response["images"].append(f'http://www.compraensanjuan.com/{src}')
    
    dict_response["description"] = soup.select_one(
        "#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-12 > p").text.strip()

    # scrap meters 

    dict_response["meters"] = {}
    dict_response["meters"]["total"] = soup.select_one(
        "#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-3.col-md-3.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_1 > p:nth-child(1) > span").text.strip()
    dict_response["meters"]["cub"] = soup.select_one(
        "#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-3.col-md-3.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_1 > p:nth-child(2) > span").text.strip()
    
    # scrap basic info

    dict_response["info_basic"] = {}
    dict_response["info_basic"]["rooms"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-3.col-md-3.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_4 > p:nth-child(1) > span").text.strip()
    dict_response["info_basic"]["garages"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-3.col-md-3.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_3 > p:nth-child(2) > span").text.strip()
    dict_response["info_basic"]["baths"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-3.col-md-3.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_2 > p:nth-child(2) > span").text.strip()
    dict_response["info_basic"]["orientation"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-3.col-md-3.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_4 > p:nth-child(2) > span").text.strip()
    dict_response["info_basic"]["neightboor"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.ubicacion-block-col.p-0 > div:nth-child(2) > div.col-lg-4.col-md-4.col-sm-4.col-12.mx-auto.caracteristica-inmueble_col_5 > p:nth-child(2) > span").text.strip()
    dict_response["info_basic"]["department"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.ubicacion-block-col.p-0 > div:nth-child(2) > div.col-lg-4.col-md-4.col-sm-4.col-12.mx-auto.caracteristica-inmueble_col_6 > p:nth-child(2) > span").text.strip()

    # scrap additionals

    dict_response["additionals"] = {}
    dict_response["additionals"]["credit"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-6.col-md-6.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_1 > p > span").text.strip()
    dict_response["additionals"]["private_neightboor"] = soup.select_one("#principal-anuncio > div > div.col-md-12.col-sm-12.col-12 > div.anuncio-block-img > div > div > div.col-lg-12.col-md-12.col-sm-12.col-12.caracteristicas-block-col.p-0 > div > div.col-lg-6.col-md-6.col-sm-6.col-12.mx-auto.caracteristica-inmueble_col_2 > p > span").text.strip()
    
    return dict_response

def scrapper_redinmo(url):
    # scrap redinmo.com.ar
    dict_response = {}
    r = requests.get(url)
    soup = BeautifulSoup(r.text, "html.parser")
    
    title = soup.select_one(
        "body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(7) > h2").text.strip()
    if "Venta" in title:
        title = title.replace("Venta |","")
    elif "Alquiler" in title:
        title = title.replace("Alquiler |","")

    dict_response["title"] = title.strip()

    dict_response["subtitle"] = soup.select_one("body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(2) > div.panel.panel-default > div > span > span").text.split("\n")[0].replace("Departamento:","").strip()

    dict_response["price"] = "$ - CONSULTAR"
    
    condition = soup.select_one("body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(7) > h2").text
    if "Venta" in condition:
        condition = "Venta"
    elif "Alquiler" in condition:
        condition = "Alquiler"
    dict_response["condition"] = condition.strip() 

    # get images URLs
    images = []
    images.extend(soup.select(".popup-img"))
    dict_response["images"] = []
    for img in images:
        src = img.attrs["href"][2:]
        dict_response["images"].append(f'http://www.redinmo.com.ar/{src}')

    dict_response["description"] = soup.select_one(
        "body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(1) > div:nth-child(3) > div > span").text.strip()

    # scrap meters

    dict_response["meters"] = {}
    dict_response["meters"]["total"] = soup.select_one("body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(2) > div.panel.panel-default > div > span > span > span > span > span").text.split("\n")[1].replace("M2 terreno: ", "").strip()
    dict_response["meters"]["cub"] = soup.select_one("body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(2) > div.panel.panel-default > div > span > span > span > span > span > span").text.split("\n")[1].replace("M2 cubiertos:", "").strip()

    # scrap basic info

    dict_response["info_basic"] = {}

    dict_response["info_basic"]["rooms"] = soup.select_one(
        "body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(2) > div.panel.panel-default > div > span > span > span").text.split("\n")[1].replace("Cantidad de Dormitorios:", "").strip()

    dict_response["info_basic"]["garages"] = "-"

    dict_response["info_basic"]["baths"] = soup.select_one("body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(2) > div.panel.panel-default > div > span > span > span > span > span").text.split("\n")[0].replace("Cantidad de Baños:","").strip()

    dict_response["info_basic"]["orientation"] = "-"

    dict_response["info_basic"]["neightboor"] = "-"

    dict_response["info_basic"]["department"] = soup.select_one("body > div.page-wrapper > section > div > div:nth-child(4) > div:nth-child(8) > div.main.col-md-8 > div > div > div:nth-child(2) > div.panel.panel-default > div > span > span").text.split("\n")[0].replace("Departamento:","").strip()

    # scrap additionals

    dict_response["additionals"] = {}
    dict_response["additionals"]["credit"] = "-"
    dict_response["additionals"]["private_neightboor"] = "-"

    return dict_response
