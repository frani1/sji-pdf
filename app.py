from flask import Flask, redirect, url_for, render_template, request, make_response
from pdf_generator import pdf_generator
from scrapper import scrapper


app = Flask(__name__)

@app.route('/', methods=["GET","POST"])
def index():
  if request.method == "GET":
    return render_template('index.html')
  else:
    url_website = request.form["url"]
    scrap_dict = scrapper(url_website)
    if "Error" in scrap_dict:
      response = render_template("err.html", message=scrap_dict["message"])
    else:
      response = pdf_generator(scrap_dict)

    # response = make_response(pdf)
    # response.headers["Content-Type"] = "application/pdf"
    # response.headers["Content-Disposition"] = f"inline; filename={filename}.pdf"

    return response

if __name__ == '__main__':
  app.run()
