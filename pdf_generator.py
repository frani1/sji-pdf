from flask import render_template
from weasyprint import HTML, CSS

# #from weasyprint import HTML, CSS
# from weasyprint.fonts import FontConfiguration

# font_config = FontConfiguration()
# html = HTML(string='<h1>The title</h1>')
# css = CSS(string='''
#     @font-face {
#         font-family: Gentium;
#         src: url(http://example.com/fonts/Gentium.otf);
#     }
#     h1 { font-family: Gentium }''', font_config=font_config)
# html.write_pdf(
#     '/tmp/example.pdf', stylesheets=[css],
#     font_config=font_config)

def pdf_generator( input_dict = {}):
    # function recive a dict with data from scrapper and render the PDF with headers
    # after that, return the pdf ready to sent as response
    dict_default = {
        "title": "",
        "subtitle": "", 
        "price": "-",
        "condition": "-",
        "images": [""],
        "info_basic": {
            "rooms": "-",
            "baths": "-",
            "garages": "No",
            "neightboor": "-",
            "department": "-",
        },
        "meters": {
            "total": "-",
            "cub": "-"
        },
        "additionals": {
            "credit": "-",
            "private_neightboor": "-"
        },
        "description": "-",
    }
    # check and set default values for keys
    for key in dict_default:
        if not key in input_dict:
            input_dict[key] = dict_default[key]
    # check and set default values for info_basic
    for key in dict_default["info_basic"]:
        if not key in input_dict["info_basic"] or input_dict["info_basic"][key] == "":
            input_dict["info_basic"][key] = dict_default["info_basic"][key]
    # check and set default values for meters
    for key in dict_default["meters"]:
        if not key in input_dict["meters"] or input_dict["meters"][key] == "":
            input_dict["meters"][key] = dict_default["meters"][key]
    # check and set default values for additionals
    for key in dict_default["additionals"]:
        if not key in input_dict["additionals"] or input_dict["additionals"][key] == "":
            input_dict["additionals"][key] = dict_default["additionals"][key]

    rendered = render_template("template_one.html", title=input_dict["title"], subtitle=input_dict["subtitle"], price=input_dict["price"], condition=input_dict["condition"], images=input_dict["images"], info_basic=input_dict["info_basic"], meters=input_dict["meters"], additionals=input_dict["additionals"], description=input_dict["description"])

    # pdf = HTML(string=rendered).write_pdf()

    return rendered
